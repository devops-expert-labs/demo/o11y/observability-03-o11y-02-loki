#!/bin/bash

# The base URL of your Nginx server
BASE_URL="http://localhost:8080"

# Define an array of paths
URIS=("/path1" "/path2" "/path3" "/path4")

# Loop indefinitely
while true; do
    # Generate a random number between 1 and 4 for the HTTP method
    METHOD_NUM=$((RANDOM % 4 + 1))

    # Assign an HTTP method based on the random number
    case $METHOD_NUM in
        1)
            METHOD="GET"
            ;;
        2)
            METHOD="POST"
            ;;
        3)
            METHOD="PUT"
            ;;
        4)
            METHOD="DELETE"
            ;;
    esac

    # Generate a random number between 0 and 3 for the path selection
    URI_NUM=$((RANDOM % 4))

    # Select a path based on the random number
    URL="${URIS[$URI_NUM]}"

    # Combine base URL and path to form the full URL
    FULL_URL="$BASE_URL$URL"

    # Make the HTTP request with the chosen method and path
    echo "Making a $METHOD request to $FULL_URL"
    curl -X $METHOD $FULL_URL
    echo -e "\nRequest made with $METHOD method to $URL\n"

    # Wait for a second before making the next request
    sleep 1
done
